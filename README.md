# README #

## Installation ##

Clone this repo, create a virtual environment over Python3, install the dependencies and migrate the DB. In short:

```
git clone https://mvolarik@bitbucket.org/mvolarik/django_user_profiles_app_example.git
cd django_user_profiles_app_example
virtualenv -p python3 .venv
source .venv/bin/activate
pip install -r requirements.txt
./user_profiles_example/manage.py migrate
```

## Running the server ##

You need to create an superuser in order to access to the Django Admin app and then, you can start the server:

```
./user_profiles_example/manage.py createsuperuser
./user_profiles_example/manage.py runserver
```

## Internals ##
A new model Profile with a OneToOne link to User was created. Signals are used to create, update and delete a Profile when a User is created, updated or deleted.
The Admin was extended with the fields of this new class.

### Playing with profiles ###

```
>>> from user_profiles.models import Profile
>>> from django.contrib.auth.models import User

>>> User.objects.all()
<QuerySet []>
>>> Profile.objects.all()
<QuerySet []>

>>> User.objects.create(username='Felipa')
<User: Felipa>
>>> Profile.objects.all()
<QuerySet [<Profile: User Profile for: Felipa>]>

>>> felipa = User.objects.get(id=1)
>>> felipa.profile
<Profile: User Profile for: Felipa>

>>> felipa.delete()
(2, {'admin.LogEntry': 0, 'auth.User_groups': 0, 'auth.User_user_permissions': 0, 'user_profiles.Profile': 1, 'auth.User': 1})
>>> Profile.objects.all()
<QuerySet []>
```

### Admin ###

![](https://bitbucket.org/mvolarik/django_user_profiles_app_example/raw/585fec14751768250fc3874c44ddfc4f47d836ea/docs/Admin%20-%20User%20details.png)
![](https://bitbucket.org/mvolarik/django_user_profiles_app_example/raw/685b63573744c76b1be57b78ccb0436033ae774c/docs/Admin%20-%20User%20list.png)
